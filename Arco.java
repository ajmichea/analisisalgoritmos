/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Iterator;
/**
 *
 * @author OscarPalomino-AntonioJimenez-AngeloArancibia
 */
public class Arco extends Nodo{
    String origen;
    String destino;
    int peso;
    private Arco adyacentes;
    private Nodo nodos;

    //Constructores
    public Arco(String origen, String destino, int peso, String nombre, Arco adyacentes, Nodo nodos) {
        super(nombre, adyacentes, nodos);
        this.origen = origen;
        this.destino = destino;
        this.peso = peso;
    }


    
    
    //Metodos
    public String getNombre(){
        return this.nombre;
    }
    
    public Arco getAdyacentes(){
        return this.adyacentes;
    }
    public int ActualizaPeso(int pes){
        this.peso = pes;
        return this.peso;
    }
}
