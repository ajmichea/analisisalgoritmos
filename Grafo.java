/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Iterator;
import java.io.*;
import java.util.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 *
 * @author OscarPalomino-AntonioJimenez-AngeloArancibia
 */
public class Grafo{
    Nodo nodos;
    String nombre;
    Nodo siguiente;
    Arco destino;
    Arco origen;
    Arco adyacentes;
    Arco arcos;

    // constructores
    public Grafo(Nodo nodos) {
        this.nodos = nodos;
    }

    //metodos
    public void CreaNodo(String nom, Arco ady, Nodo nnodo){
        Nodo NuevoNodo = new Nodo(nom, ady, nnodo);
        this.nodos = nnodo;
        this.nombre = nom;
        this.adyacentes= ady;
    }
    public void EliminaNodo(Nodo nodo){
        nodo.adyacentes=null;
        nodo.nodos=null;
    }
    public boolean BuscarNodo(Nodo nodo,String a){
        while (nodo!=null) {
            if(nodos.nombre.equals(a)) {
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    public void ListarNodo(){ // void por Nodo
        
        while(nodos.siguiente!= null){
            System.out.println("Nombre: "+this.nodos.nombre);
            System.out.println("Peso: "+this.arcos.peso);
            System.out.println("-------------------------");
            nodos.siguiente=nodos;
        }
    }
}
